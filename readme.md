###COMANDOS BÁSICOS:

docker images 
docker ps
docker ps -a
docker images
docker search ubuntu
docker rmi ID_ou_nome_da_imagem
docker rm id_ou_apelido
docker-compose up -d

###Na pasta raiz do projeto rodar o comando para fazer pull/rodar o portainer (após isso você poderá acessar o portainer pelo localhost:9000)

docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer

###Entre na pasta php_7_2 e rode comando abaixo, faça o mesmo dentro da pasta php_5_6

docker-compose up -d

###Pronto, você terá acesso à:

php 7.2 na porta 80 acessando locahost
php 5.6 na porta 5080 acessando localhost:5080
hpmyadmin na porta 6080 acessando localhost:6080
mysql (veja o ip no portainer). Usuário: root, Senha: teste123